using Microsoft.AspNetCore.Mvc;

namespace OdeToFood.Controllers
{
    [Route("[controller]")]
    public class AboutController
    {
        public string Phone()
        {
            return "48 9 99885982";
        }

        public string Adress()
        {
            return "Brasil";
        }
    }
}