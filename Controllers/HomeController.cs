using Microsoft.AspNetCore.Mvc;
using OdeToFood.Models;
using OdeToFood.Services;
using OdeToFood.ViewModels;

namespace OdeToFood.Controllers
{
    //[Route("[controller]/[action]/{id?}")] com esse código não estava respeitando rota default
    public class HomeController : Controller
    {
        private IRestaurantData _restaurantData;
        private IGreeter _greeter;

        public HomeController(IRestaurantData restaurantData, IGreeter greeter)
        {
            _restaurantData = restaurantData;
            _greeter = greeter;
        }

        public IActionResult Index()
        {
            var  model = new HomeIndexViewModel();
            model.Restaurants = _restaurantData.GetAll();
            model.CurrentMessage = _greeter.GetMessageOfTheDay();
            // return new ObjectResult(model); //para utilizar em API, serializado o objeto em JSON 
            return View(model);
        }

        public IActionResult Details(int id)
        {
            var model = _restaurantData.Get(id);
            if (model == null)
            {
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost] //verificar porque não cai aqui quando clica para salvar 
        //[ValidateAntiForgeryToken] //quando usar cookies, sempre usar este atributo, para verificar o RequestVerificationToken do browser (para evitar cross-site request forgery) 
        public IActionResult Create(RestaurantEditModel model)
        {
            if (ModelState.IsValid)
            {
                //criado o RestaurantEditModel para garantir que eu pegue apenas os dados que eu preciso. 
                //isso para evitar overposting, técnica maliciosa onde um agente externo "injeta" coisas
                //em propriedades que o código não estava esperando. Isso porque o MVC sempre tenta preencher
                //todas as propriedades automaticamente. 
                var newRestaurant = new Restaurant();
                newRestaurant.Name = model.Name;
                newRestaurant.Cuisine = model.Cuisine;

                newRestaurant = _restaurantData.Add(newRestaurant);
                return View("Details", newRestaurant);
            }
            else
            {
                return View();
            }
        }
    }
}