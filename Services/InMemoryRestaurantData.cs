using System.Collections.Generic;
using System.Linq;
using OdeToFood.Models;

namespace OdeToFood.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        public InMemoryRestaurantData()
        {
            _restaurant = new List<Restaurant>
            {
                new Restaurant {Id = 1, Name = "Restaurante da Andressa"},
                new Restaurant {Id = 2, Name = "Restaurante do Rodrigo"},
                new Restaurant {Id = 3, Name = "Restaurante do Blue"},
                new Restaurant {Id = 3, Name = "Restaurante do Pequeno"}
            };
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurant.OrderBy(r => r.Name);
        }

        public Restaurant Get(int Id)
        {
            return _restaurant.FirstOrDefault(r => r.Id == Id);
        }

        public Restaurant Add(Restaurant restaurant)
        {
            restaurant.Id = _restaurant.Max(r => r.Id) + 1;
            _restaurant.Add(restaurant);
            return restaurant;
        }

        List<Restaurant> _restaurant;
    }
}